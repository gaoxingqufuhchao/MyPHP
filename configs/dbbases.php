<?php
    /**
    * 数据库的基本配置
     */
  return array(
      // 主数据库配置
      "master" => [
          "type" => "mysqli",
          "host" => "127.0.0.1",
          "user" => "root",
          "password" => "root",
          "dbname" => "runmoney2",
          "prefix" => "rm_"
      ],

      // 从数据库群
      "slave" => [
          [
              "type" => "mysql",
              "host" => "127.0.0.1",
              "user" => "root",
              "password" => "root",
              "dbname" => "test",
              "prefix" => "rm_"
          ],
          [
              "type" => "mysqli",
              "host" => "127.0.0.1",
              "user" => "root",
              "password" => "root",
              "dbname" => "test",
              "prefix" => "rm_"
          ],
          [
              "type" => "pdo",
              "host" => "127.0.0.1",
              "user" => "root",
              "password" => "root",
              "dbname" => "test",
              "prefix" => "rm_"
          ],
      ]

  );

?>