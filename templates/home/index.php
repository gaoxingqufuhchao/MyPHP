<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
		<meta name="format-detection" content="telphone=no, email=no" />
		<title><?php echo $title ?></title>
		<style>
			.font-size-tong {
                font-size: 0.32rem;
            }
			.font-size-tong2 {
				font-size: 0.26rem;
			}
			
			.container {
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, .5);
				padding-top: 2rem;
				box-sizing: border-box;
			}
			
			body {
				width: 100%;
				height: 100%;
				overflow: hidden;
				background: url(images/hb.png) no-repeat no-repeat;
				background-size: cover;
			}
			
			input::-webkit-input-placeholder {
				/* WebKit browsers*/
				color: #FFF;
				font-size: 0.28rem;
				line-height: 0.5rem;
			}
			
			input:-moz-placeholder {
				/* Mozilla Firefox 4 to 18*/
				color: #fff;
				font-size: 0.28rem;
				line-height: 0.5rem;
			}
			
			input::-moz-placeholder {
				/* Mozilla Firefox 19+*/
				color: #fff;
				font-size: 0.28rem;
				line-height: 0.5rem;
			}
			
			input:-ms-input-placeholder {
				/* Internet Explorer 10+*/
				color: #fff;
				font-size: 0.28rem;
				line-height: 0.5rem;
			}
			
			.img {
				width: 100%;
				height: 2rem;
				margin-bottom: .2rem;
			}
			
			.img img {
				height: 100%;
			}
			
			.layui-m-layercont {
				height: 8rem;
				overflow-y: auto;
			}
			.layui-m-layermain>.layui-m-layersection>.layui-m-layercont>.layui-m-layercont { 
				text-align:left;
			}
			a { text-decoration: none;color:#fff;}
		</style>
	</head>

	<body>
<!--        <ul>-->
<!--            --><?php
//                foreach ($dates as $key => $val) {
//                    echo "<li>姓名: ".$val['name']." 年龄: ".$val['age']."</li>";
//                }
//            ?>
<!--        </ul>-->

		<div class="container">
			<section class="form_box">
				<p>
					<input class="font-size-tong" id='ph' type="text" placeholder="请输入手机号码" onkeypress="return event.keyCode>=48&&event.keyCode<=57" onkeyup="value=value.replace(/[^\d]/g,'')" />
				</p>
				<!--<p class="verfiy_code">
					<input class="font-size-tong" type="text" id='cd' placeholder="请输入手机验证码" onkeypress="return event.keyCode>=48&&event.keyCode<=57" ng-pattern="/[^a-zA-Z]/" />
					<button id="getCode" class="font-size-tong2" onclick="sendMsg();">获取验证码</button>
				</p>-->
				<p>
					<input class="font-size-tong" type="password" id='pw' placeholder="密码6-16位密码" />
				</p>
				<div class="register_btn">
					<input type="button" value="注     册" onclick="postRegister();" />
				</div>

				<div style="font-size:0.28rem;margin-top:0.2rem;color:#fff;">
					<input type="checkbox" id="files_user" name="user_file" style="zoom:100%;vertical-align: middle;" checked="true"/>
					<label for="files_user">请认真阅读 <a href="javascript:readFile(this)">《跑步钱进用户协议》</a></label>
				</div>
				<div class="register_btn">
					<a href="dl.html">
						<input type="button" value="已注册直接下载" />
					</a>
				</div>

			</section>
		</div>
	</body>

</html>