<?php
/**
* 用户模型类
 * 获取用户实际数据
 */

namespace app\model;


class User extends \myphp\Model
{

    public function getUserInfo()
    {
        //return get_class();
        $data = [
            "id" => 10,
            "nickname" => "好样的",
            "avator" => "http://www.baidu.com/",
        ];
        return $data;
    }

    /**
    * 用户注册
     * 场景: 当用户注册成功后需要执行后面操作
     * 解决方法: 添加监听者，当注册成功则里面的监听者根据变动自动改变逻辑
     */
    public function create()
    {
        echo "用户注册成功了<br/>";
        $this->notify();
    }

}