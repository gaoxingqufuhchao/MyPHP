<?php
/**
* 控制器类模板装饰
 *
 */

namespace app\decorator;


class Template implements \myphp\Decorator
{
    protected $obj;

    // 传入
    public function beforeAction($obj)
    {
        $this->obj = $obj;
        echo "第一步执行开始前置方法<br/><br/>";
    }

    public function afterAction($data)
    {
        // TODO: Implement afterAction() method.
    }

}