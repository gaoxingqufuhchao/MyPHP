# MyPHP

#### 介绍
根据几种常见的设计模式：单例模式，工厂模式，观察者模式，装饰器模式，注册树模式，代理模式，以及用到了ArrayAccess等PHP的预定义接口类。手写的一套PHP MVC框架----MyPHP，有借鉴imooc例子，后期也完善路由等功能。

#### 目录结构介绍

www  WEB部署目录（或者子目录）
├─app                   应用目录
│  ├─controller         应用控制器
│  ├─model              应用模型
│  ├─decorator          应用控制器的装饰器
│  └─observer           应用模型的观察者
│
├─configs               应用配置目录
│  ├─controller.php     控制器配置(添加装饰器等)
│  ├─dbbases.php        数据库配置
│  └─model.php          模型配置(添加观察者等)
│
├─public                WEB目录（对外访问目录）
│  ├─index.php          入口文件
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─myphp                 框架系统目录
│  ├─databases          数据库连接方式类
│  │  ├─proxy.php       代理连接(实现读写分离)
│  │  └─Mysqli.php      mysqli方式
│  │
│  ├─Application.php    框架入口基类
│  ├─AutoLoader.php     自动加载类
│  ├─Configs.php        配置文件读取类(让对象像数组一样使用)
│  ├─Controller.php     控制器基类
│  ├─Database.php       数据库连接抽象类
│  ├─Model.php          模型基类(装载模型上对应的观察者)
│  ├─Observer.php       观察者接口类
│  ├─Register.php       注册树类(存储常用不变更类的实例)
│  ├─Decorator.php      装饰器接口类(约束子类必须完成几个方法)
│  └─Factory.php        封装常用业务逻辑实例的工厂类
│
├─templates             应用视图模板文件
├─runtime               应用的运行时目录（暂未做）
├─vendor                第三方类库目录
├─index.php             项目入口文件


#### 公众号
![输入图片说明](https://images.gitee.com/uploads/images/2019/0529/172213_03fa9070_776822.jpeg "164234_pf7y_2456768.jpg")
