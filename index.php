<?php
    /**
    * author: fuchao
     * date: 20190527
     * desc: 项目的入口文件
     */

    // 定义项目常量
    const DS = DIRECTORY_SEPARATOR;     // 斜杠的php预定义常量
    const BASEDIR = __DIR__;            // 项目的根路径

    // 引入自动加载类
    require "myphp/AutoLoader.php";
    // 通过自动加载函数加载类
    spl_autoload_register('\myphp\AutoLoader::autoLoader');

    // 开始执行框架的基类
    \myphp\Application::getInstance(BASEDIR )->dispatch();



