<?php
/**
* 控制器的装饰器接口类
 *
 */

namespace myphp;


interface Decorator
{

    // 控制器类前置方法
    public function beforeAction($obj);

    // 控制器类后置方法
    public function afterAction($data);
}