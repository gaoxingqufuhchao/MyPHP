<?php


namespace myphp;


abstract class Database
{
    abstract function query($sql);
}