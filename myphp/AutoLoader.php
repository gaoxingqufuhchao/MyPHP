<?php

namespace myphp;

class AutoLoader
{
    public static function autoLoader($classname) {
        $calssFile = file_exists(BASEDIR.DS.$classname.".php");

        if($calssFile) {
            require $classname.".php";
        }else {
            echo "<h2>$classname.php 类不存在</h2>";
            die;
        }

    }
}