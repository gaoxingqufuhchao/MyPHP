<?php
/**
* 实现ArrayAccess接口类：让其实例的对象可以当成数组一样使用
 * 注: ArrayAccess接口类是PHP5以来新增的预定义类, 其中几个方法必须完成
 * offsetGet: 当外部通过数组的键使用该对象时被调用
 * offsetSet
 * offsetUnset
 * offsetExists
 */

namespace myphp;


class Configs implements \ArrayAccess
{
    public function offsetGet($key) {
        if($key) {
            return require BASEDIR.DS."configs".DS.$key.".php";
        }
    }

    public function offsetSet($key,$value) {
        return "123";
    }

    public function offsetExists($key) {
        return "已经存在";
    }

    public function offsetUnset($key) {
        return "删除";
    }

}