<?php
/**
* 注册树模式
 * 将常用对象保存到全局静态数组中
 */

namespace myphp;


class Register
{
    protected static $objects;

    // 保存实例到全局容器中
    public static function set($key,$obj)
    {
        return self::$objects[$key] = $obj;
    }

    // 从容器中获取实例
    public static function get($key)
    {
        if(!self::$objects[$key]) {
            return false;
        }
        return self::$objects[$key];
    }

    // 从容器中去除实例
    public static function unsets($key)
    {
        unset(self::$objects[$key]);
    }

}