<?php
/**
* 代理模式-数据库代理类
 */

namespace myphp\databases;


use myphp\Database;

class Proxy extends Database
{

    // 完成父级抽象类的抽象方法
    public function query($sql)
    {
        $sqlStr = substr(trim($sql),0,6);
        // 执行读操作，访问主数据库
        if($sqlStr == "select") {
            return Factory::getDb('master')->query($sql);
        }else {
            return Factory::getDb('salve')->query($sql);
        }
    }

}