<?php
/**
* Mysql类型数据库类
*/
namespace myphp\databases;


class Mysql extends \myphp\Database
{
    protected $conn;
    public function connect($host, $user, $password, $dbname)
    {
        //return 12345678;
        $conn = mysql_connect($host, $user, $password);
        mysql_select_db($dbname,$conn);
        $this->conn = $conn;
    }

    // 数据库查询
    public function query($sql)
    {
        //return 'hello worlds fuchao';
        mysql_query($sql,$this->conn);
    }

    // 关闭数据库
    public function close() {
        mysql_close($this->conn);
    }

}