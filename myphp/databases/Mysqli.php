<?php


namespace myphp\databases;


use myphp\Database;

class Mysqli extends Database
{
    protected $conn;
    public function connect($host, $user, $password, $dbname) {
        $conn = mysqli_connect($host,$user,$password,$dbname);
        $this->conn = $conn;
    }

    public function query($sql) {
        return mysqli_query($this->conn,$sql);
    }

    public function close() {
        return mysqli_close($this->conn);
    }
}