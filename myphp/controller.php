<?php
/**
* 框架控制器基类: 封装控制器的常用方法
 * display：
 * assign：
 */

namespace myphp;


class controller
{
    protected $controller;
    protected $action;
    protected $templateDir;
    protected $data;            // 传递数据

    // 初始化控制器名称
    function __construct($controller,$action)
    {
        $this->controller = $controller;
        $this->action = $action;
        $this->templateDir = BASEDIR.DS.'templates'.DS;
    }

    // display方法用于显示模板
    public function display($file="") {
        if(empty($file)) {
            $template = $this->templateDir.$this->controller.DS.$this->action.'.php';
        }else {
            $template = $this->templateDir.$file.DS.'.php';
        }
        extract($this->data);
        include $template;
    }

    // assign方法用于在模板中显示数据
    public function assign($key,$data) {
        $this->data[$key] = $data;
    }


}