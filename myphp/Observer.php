<?php
/**
* 观察者接口类，定义观察者的执行操作
 */

namespace myphp;


interface Observer
{
    public function update();
}