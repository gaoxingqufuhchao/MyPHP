<?php
/**
* 框架的模型基类
 * 观察者模式
 */

namespace myphp;


class Model
{
    protected $observer;

    public function __construct()
    {
        // 获取当前模型类的类名
        $className = strtolower(str_replace("app\model\\","",get_called_class()));

        // 读出当前模型的所有观察者类(从配置文件中)
        $modelConfigs = Application::getInstance(BASEDIR)->config['model']['observers'][$className];

        if($modelConfigs) {
            foreach ($modelConfigs as $key=>$value) {
                $this->observer[] = new $value();
            }
        }
    }

    // 回调
    public function notify() {
        foreach ($this->observer as $key => $value) {
            $value->update();
        }
    }

}